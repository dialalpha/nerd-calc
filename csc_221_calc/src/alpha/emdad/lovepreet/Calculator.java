package alpha.emdad.lovepreet;

public class Calculator {
	
	protected static final long MAX_SIGNED = (long)(Math.pow(2.0, 31.0) - 1);
	protected static final long MIN_SIGNED = -1 * (long)(Math.pow(2.0,31.0));
	protected static final long MAX_UNSIGNED = (long)(Math.pow(2.0, 32.0) - 1);
	static int tmp;
	
	protected static void updateCurrentNum(long digit){
		if (IntegerActivity.done == true)
			IntegerActivity.done = false;
		
		if(IntegerActivity.numberBase == 0){
			if(IntegerActivity.currentNum == 1)
				IntegerActivity.num1 = IntegerActivity.num1 * 2 + digit;
			else if (IntegerActivity.currentNum == 2)
				IntegerActivity.num2 = IntegerActivity.num2 * 2 + digit;
			removeOverflow();
			Display.showResults();
			return;
		}//end if for base 2
		
		if(IntegerActivity.numberBase == 1){
			if(IntegerActivity.currentNum == 1)
				IntegerActivity.num1 = IntegerActivity.num1 * 10 + digit;
			else if (IntegerActivity.currentNum == 2)
				IntegerActivity.num2 = IntegerActivity.num2 * 10 + digit;
			removeOverflow();
			Display.showResults();
			return;
		}//end if for base 10
		
		if(IntegerActivity.numberBase == 2){
			if(IntegerActivity.currentNum == 1)
				IntegerActivity.num1 = IntegerActivity.num1 * 16 + digit;
			else if (IntegerActivity.currentNum == 2)
				IntegerActivity.num2 = IntegerActivity.num2 * 16 + digit;
			removeOverflow();
			Display.showResults();
			return;
		}//end if for base 16
		
	}//end updateCurrentNum
	
	protected static long signed_add(long num1, long num2){
		IntegerActivity.overflow = false;
		long res = num1 + num2;
		if (res > MAX_SIGNED){
			IntegerActivity.overflow = true;
			IntegerActivity.showOverflowAlert();
		}
		tmp = (int)res;			//discard MSB
		res = (long)tmp;		//sign extend
		return res;
	}//end signed_add
	
	protected static long signed_sub(long num1, long num2){
		IntegerActivity.overflow = false;
		long res = num1 - num2;
		if (res > MAX_SIGNED){
			IntegerActivity.overflow = true;
			IntegerActivity.showOverflowAlert();
		}
		tmp = (int)res;			//discard MSB
		res = (long)tmp;		//sign extend
		return res;
	}//end signed_sub
	
	protected static long signed_mul(long num1, long num2){
		long res = num1 * num2;
		if (res > MAX_SIGNED){
			IntegerActivity.overflow = true;
			IntegerActivity.showOverflowAlert();
		}
		tmp = (int)res;			//discard MSB
		res = (long)tmp;		//sign extend
		return res;
	}//end signed _mul
	
	protected static long signed_div(long num1, long num2) throws ArithmeticException{
		long res = 0;
		try{
			res = num1 / num2;
		}catch (ArithmeticException e){
			res = 0;
			IntegerActivity.showDivByZeroAlert();
		}
		return res;
	}//end signed_div
	
	protected static long unsigned_add(long num1, long num2){
		long res = num1 + num2;
		if (res > MAX_UNSIGNED){
			IntegerActivity.overflow = true;
			IntegerActivity.showOverflowAlert();
		}
		tmp = (int)res;
		res = res & 0xffffffffL;
		return res;
	}//end unsigned_add
	
	protected static long unsigned_sub(long num1, long num2){
		long ans;
		if (num1 >= num2)
			ans = num1 - num2;
		else ans = num2 - num1;
		return ans;
	}//end unsigned_sub
	
	protected static long unsigned_mul(long num1, long num2){
		long res = num1 * num2;
		if (res > MAX_UNSIGNED){
			IntegerActivity.overflow = true;
			IntegerActivity.showOverflowAlert();
		}
		tmp = (int)res;
		res = res & 0xffffffffL;
		return res;
	}//end unsigned _mul
	
	protected static long unsigned_div(long num1, long num2) throws ArithmeticException{
		long res = 0;
		try{
			res = num1 / num2;
		}catch (ArithmeticException e){
			res = 0;
			IntegerActivity.showDivByZeroAlert();
		}
		return res;
	}//end unsigned_div
	
	protected static long logical_and(long num1, long num2){
		return (num1 & num2);
	}//end signed_and
	
	protected static long logical_or(long num1, long num2){
		return (num1 | num2);
	}//end signed_or
	
	protected static long logical_not(long num){
		return ~num;
	}//end signed_not
	
	protected static long logical_xor(long num1, long num2){
		return num1 ^ num2;
	}//end signed_xor
	
	protected static long shift_left(long num){
		return num << 1;
	}//end sl
	
	protected static long shift_right(long num){
		return num >> 1;
	}
	
	protected static void negateCurrentNum(){
		if (IntegerActivity.currentNum == 0){
			IntegerActivity.ans	= -1 * IntegerActivity.ans;
		}
		else if (IntegerActivity.currentNum == 1){
			IntegerActivity.num1 = -1 * IntegerActivity.num1;
		}
		else if (IntegerActivity.currentNum == 2){
			IntegerActivity.num2 = -1 * IntegerActivity.num2;
		}
		removeOverflow();
	}
	
	protected static void removeOverflow(){
		int tmp;
		if (IntegerActivity.signed == true){
			/* chop off MSB, then sign-extend */
			if(IntegerActivity.num1 > MAX_SIGNED ||
					IntegerActivity.num2 > MAX_SIGNED ||
					IntegerActivity.ans > MAX_SIGNED)
				IntegerActivity.showOverflowAlert();
			
			tmp = (int)IntegerActivity.num1;
			IntegerActivity.num1 = (long)tmp;
			tmp = (int)IntegerActivity.num2;
			IntegerActivity.num2 = (long)tmp;
			tmp = (int)IntegerActivity.ans;
			IntegerActivity.ans = (long)tmp;
		}else if (IntegerActivity.signed == false){
			/* chop off MSB, then zero extend */
			if(IntegerActivity.num1 > MAX_UNSIGNED ||
					IntegerActivity.num2 > MAX_UNSIGNED ||
					IntegerActivity.ans > MAX_UNSIGNED)
				IntegerActivity.showOverflowAlert();
			
			tmp = (int)IntegerActivity.num1;
			IntegerActivity.num1 = tmp & 0xffffffffL;
			tmp = (int)IntegerActivity.num2;
			IntegerActivity.num2 = tmp & 0xffffffffL;
			tmp = (int)IntegerActivity.ans;
			IntegerActivity.ans = tmp & 0xffffffffL;
		}
	}
};// end class
