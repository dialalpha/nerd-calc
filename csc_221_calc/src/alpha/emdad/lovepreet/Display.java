package alpha.emdad.lovepreet;

public class Display {
	
	static String decResult, hexResult, binResult, binMSB, binLSB;
	
	public static void showResults(){
		processDecimal();
		processHex();
		processBinary();
		IntegerActivity.decimalResultTxt.setText(decResult);
		IntegerActivity.hexResultTxt.setText(hexResult);
		IntegerActivity.binaryLsbResultTxt.setText(binLSB);
		IntegerActivity.binaryMsbResultTxt.setText(binMSB);
	}
	
	public static void processDecimal(){
		if (IntegerActivity.signed == false){
			if(IntegerActivity.currentNum == 1)
				decResult = Long.toString(Math.abs(IntegerActivity.num1));
			else if (IntegerActivity.currentNum == 2)
				decResult = Long.toString(Math.abs(IntegerActivity.num2));
			else if (IntegerActivity.currentNum == 0)
				decResult = Long.toString(Math.abs(IntegerActivity.ans));
		}else{
			if(IntegerActivity.currentNum == 1)
				decResult = Long.toString(IntegerActivity.num1);
			else if (IntegerActivity.currentNum == 2)
				decResult = Long.toString(IntegerActivity.num2);
			else if (IntegerActivity.currentNum == 0)
				decResult = Long.toString(IntegerActivity.ans);
		}
	}
	
	public static void processBinary(){
		int tmp = 0, i = 0;
		String tmpStr = "";
		if(IntegerActivity.currentNum == 1)
			binResult = Integer.toBinaryString((int)IntegerActivity.num1);
		else if (IntegerActivity.currentNum == 2)
			binResult = Integer.toBinaryString((int)IntegerActivity.num2);
		else if (IntegerActivity.currentNum == 0)
			binResult = Integer.toBinaryString((int)IntegerActivity.ans);
		tmp = 32 - binResult.length();
		for(i = 0; i < tmp; ++i)
			binResult = "0" + binResult;
		binMSB = binResult.substring(0, 16);
		binLSB = binResult.substring(16, 32);
		for (i = 0; i < 16; i+=4)
			tmpStr = tmpStr + " " + binMSB.substring(i, i+4);
		binMSB = tmpStr;
		tmpStr = "";
		for (i = 0; i < 16; i+=4)
			tmpStr = tmpStr + " " + binLSB.substring(i, i+4);
		binLSB = tmpStr;
	}
	
	public static void processHex(){
		int i, tmp;
		String tmpStr;
		if(IntegerActivity.currentNum == 1)
			hexResult = Integer.toHexString((int)IntegerActivity.num1).toUpperCase();
		else if (IntegerActivity.currentNum == 2)
			hexResult = Integer.toHexString((int)IntegerActivity.num2).toUpperCase();
		else if (IntegerActivity.currentNum == 0)
			hexResult = Integer.toHexString((int)IntegerActivity.ans).toUpperCase();
		tmp = 8 - hexResult.length();
		for (i = 0; i < tmp; ++i)
			hexResult = "0" + hexResult;
		hexResult = "0x" + hexResult;
	}

}
