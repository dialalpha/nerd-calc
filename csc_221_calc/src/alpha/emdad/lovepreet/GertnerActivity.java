package alpha.emdad.lovepreet;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class GertnerActivity extends Activity {
    /** Called when the activity is first created. */
	
	Button integerBtn, floatBtn;
	Intent openFloat = new Intent("alpha.emdad.lovepreet.FLOATACTIVITY");
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        integerBtn = (Button) findViewById(R.id.intBtn);
        floatBtn = (Button) findViewById(R.id.floatBtn);
        
        integerBtn.setOnClickListener(new View.OnClickListener() {			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent openInt = new Intent(getApplicationContext(), IntegerActivity.class);	
				startActivity(openInt);
			}//end onclick method
			});//end setonclicklistener
    
	}//end oncreate method

	
}//end class
