package alpha.emdad.lovepreet;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

public class IntegerActivity extends Activity{
	
	protected static int numberBase;				//bin = 0, dec = 1, hex = 2
	protected static int currentNum;				//0, 1, 2 for ans, num1, num2, respectively
	protected static int currentOp;					// +, -, and, or, etc..
	protected static boolean signed;				//1 = signed, 0 = unsigned
	protected static long num1 = 0, num2 = 0, ans = 0;	
	protected static boolean done;					//asserted when an operation is finished.
	protected static boolean overflow;				//asserted when overflow ocurs
	
	static ToggleButton decToggle, hexToggle, binToggle, twoCompToggle;
	static Button hexD, hexE, hexF, andBtn, orBtn, notBtn;
	static Button hexA, hexB, hexC, slBtn, xorBtn, srBtn;
	static Button sevenBtn, eightBtn, nineBtn, plusBtn, minusBtn, plus_minusBtn;
	static Button fourBtn, fiveBtn, sixBtn, multBtn, divBtn;
	static Button oneBtn, twoBtn, threeBtn, zeroBtn, equalBtn, clearBtn;
	
	protected static TextView hexResultTxt, decimalResultTxt;
	protected static TextView binaryMsbResultTxt, binaryLsbResultTxt;
	protected static TextView operationTxt;
	
	static Context context;
	static CharSequence overflowAlert = "Overflow Alert! Stop the presses!!!!";
	static CharSequence divByZeroAlert = "Division by Zero will break the universe!!!";
	static int duration = Toast.LENGTH_SHORT;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.integer_layout);
		
		//Instantiate keypad components
		decToggle = (ToggleButton)findViewById(R.id.decToggle);
		hexToggle = (ToggleButton)findViewById(R.id.hexToggle);
		binToggle = (ToggleButton)findViewById(R.id.binToggle);
		hexD = (Button)findViewById(R.id.hexDBtn);
		hexE = (Button)findViewById(R.id.hexEBtn);
		hexF = (Button)findViewById(R.id.hexFBtn);
		andBtn = (Button)findViewById(R.id.andBtn);
		orBtn = (Button)findViewById(R.id.orBtn);
		notBtn = (Button)findViewById(R.id.notBtn);
		hexA = (Button)findViewById(R.id.hexABtn); 
		hexB = (Button)findViewById(R.id.hexBBtn); 
		hexC = (Button)findViewById(R.id.hexCBtn);
		slBtn = (Button)findViewById(R.id.leftBtn); 
		xorBtn = (Button)findViewById(R.id.xorBtn); 
		srBtn = (Button)findViewById(R.id.rightBtn);
		sevenBtn = (Button)findViewById(R.id.sevenBtn);
		eightBtn = (Button)findViewById(R.id.eightBtn);
		nineBtn = (Button)findViewById(R.id.nineBtn);
		plusBtn = (Button)findViewById(R.id.addBtn); 
		minusBtn = (Button)findViewById(R.id.subBtn);
		plus_minusBtn = (Button)findViewById(R.id.signBtn);
		fourBtn = (Button)findViewById(R.id.fourBtn); 
		fiveBtn = (Button)findViewById(R.id.fiveBtn); 
		sixBtn = (Button)findViewById(R.id.sixBtn);
		multBtn = (Button)findViewById(R.id.multiplicationBtn);
		divBtn = (Button)findViewById(R.id.divisionBtn); 
		twoCompToggle = (ToggleButton)findViewById(R.id.twocompToggle);
		oneBtn = (Button)findViewById(R.id.oneBtn);
		twoBtn = (Button)findViewById(R.id.twoBtn);
		threeBtn = (Button)findViewById(R.id.threeBtn);
		zeroBtn = (Button)findViewById(R.id.zeroBtn);
		equalBtn = (Button)findViewById(R.id.enterBtn);
		clearBtn = (Button)findViewById(R.id.clearBtn);
		
		//Instantiate display components
		hexResultTxt = (TextView)findViewById(R.id.hexOutput); 
		decimalResultTxt = (TextView)findViewById(R.id.decOutput);
		binaryMsbResultTxt = (TextView)findViewById(R.id.binaryMSB_out); 
		binaryLsbResultTxt = (TextView)findViewById(R.id.binaryLSB_out);
		operationTxt = (TextView)findViewById(R.id.operationOutput);	
		
		//Set up initial state
		done = true;
		overflow = false;
		signed = false;
		currentNum = 1;
		twoCompToggle.setChecked(false);
		plus_minusBtn.setEnabled(false);
		gotoDecimalState();
		setOnClickListeners();
	}// end onCreate
	
	/* (non-Javadoc)
	 * @see android.app.Activity#onResume()
	 */
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		context = getApplicationContext();
		Display.showResults();
	}

	//static functions
	static void gotoDecimalState() {
		numberBase = 1;
		if(!(decToggle.isChecked()))
			decToggle.setChecked(true);
		binToggle.setChecked(false);
		hexToggle.setChecked(false);
		hexA.setEnabled(false);
		hexB.setEnabled(false);
		hexC.setEnabled(false);
		hexD.setEnabled(false);
		hexE.setEnabled(false);
		hexF.setEnabled(false);
		twoBtn.setEnabled(true);
		threeBtn.setEnabled(true);
		fourBtn.setEnabled(true);
		fiveBtn.setEnabled(true);
		sixBtn.setEnabled(true);
		sevenBtn.setEnabled(true);
		eightBtn.setEnabled(true);
		nineBtn.setEnabled(true);
	}//end gotoDecimal State
	
	static void gotoBinaryState(){
		numberBase = 0;
		if(!(binToggle.isChecked()))
			binToggle.setChecked(true);
		decToggle.setChecked(false);
		hexToggle.setChecked(false);
		hexA.setEnabled(false);
		hexB.setEnabled(false);
		hexC.setEnabled(false);
		hexD.setEnabled(false);
		hexE.setEnabled(false);
		hexF.setEnabled(false);
		twoBtn.setEnabled(false);
		threeBtn.setEnabled(false);
		fourBtn.setEnabled(false);
		fiveBtn.setEnabled(false);
		sixBtn.setEnabled(false);
		sevenBtn.setEnabled(false);
		eightBtn.setEnabled(false);
		nineBtn.setEnabled(false);
	}//end gotoBinary state
	
	static void gotoHexadecimalState(){
		numberBase = 2;
		if (!(hexToggle.isChecked()))
			hexToggle.setChecked(true);
		decToggle.setChecked(false);
		binToggle.setChecked(false);
		hexA.setEnabled(true);
		hexB.setEnabled(true);
		hexC.setEnabled(true);
		hexD.setEnabled(true);
		hexE.setEnabled(true);
		hexF.setEnabled(true);
		twoBtn.setEnabled(true);
		threeBtn.setEnabled(true);
		fourBtn.setEnabled(true);
		fiveBtn.setEnabled(true);
		sixBtn.setEnabled(true);
		sevenBtn.setEnabled(true);
		eightBtn.setEnabled(true);
		nineBtn.setEnabled(true);
	}//end gotoHex state
	
	//OnClickListeners for all buttons in activity
	static void setOnClickListeners(){
		//------------------------state switching buttons----------------//
		hexToggle.setOnClickListener(new View.OnClickListener(){
			public void onClick(View v) {
				gotoHexadecimalState();
			}	
		});
		
		decToggle.setOnClickListener(new View.OnClickListener(){
			public void onClick(View v) {
				gotoDecimalState();
			}	
		});
		
		binToggle.setOnClickListener(new View.OnClickListener(){
			public void onClick(View v) {
				gotoBinaryState();
			}	
		});
		
		twoCompToggle.setOnClickListener(new View.OnClickListener(){
			public void onClick(View v) {
				if(twoCompToggle.isChecked()){
					signed = true;
					plus_minusBtn.setEnabled(true);
				} else if (!(twoCompToggle.isChecked())){
					signed = false;
					plus_minusBtn.setEnabled(false);
				}//end if
				Calculator.removeOverflow();
				Display.showResults();
			}	
		});
		
		//---------------number buttons------------------//
		zeroBtn.setOnClickListener(new View.OnClickListener(){
			public void onClick(View v) {
				Calculator.updateCurrentNum(0L);
			}	
		});
		
		oneBtn.setOnClickListener(new View.OnClickListener(){
			public void onClick(View v) {
				Calculator.updateCurrentNum(1L);
			}	
		});
		
		twoBtn.setOnClickListener(new View.OnClickListener(){
			public void onClick(View v) {
				Calculator.updateCurrentNum(2L);
			}	
		});
		
		threeBtn.setOnClickListener(new View.OnClickListener(){
			public void onClick(View v) {
				Calculator.updateCurrentNum(3L);
			}	
		});
		
		fourBtn.setOnClickListener(new View.OnClickListener(){
			public void onClick(View v) {
				Calculator.updateCurrentNum(4L);
			}	
		});
		
		fiveBtn.setOnClickListener(new View.OnClickListener(){
			public void onClick(View v) {
				Calculator.updateCurrentNum(5L);
			}	
		});
		
		sixBtn.setOnClickListener(new View.OnClickListener(){
			public void onClick(View v) {
				Calculator.updateCurrentNum(6L);
			}	
		});
		
		sevenBtn.setOnClickListener(new View.OnClickListener(){
			public void onClick(View v) {
				Calculator.updateCurrentNum(7L);
			}	
		});
		
		eightBtn.setOnClickListener(new View.OnClickListener(){
			public void onClick(View v) {
				Calculator.updateCurrentNum(8L);
			}	
		});
		
		nineBtn.setOnClickListener(new View.OnClickListener(){
			public void onClick(View v) {
				Calculator.updateCurrentNum(9L);
			}	
		});
		
		hexA.setOnClickListener(new View.OnClickListener(){
			public void onClick(View v) {
				Calculator.updateCurrentNum(10L);
			}	
		});
		
		hexB.setOnClickListener(new View.OnClickListener(){
			public void onClick(View v) {
				Calculator.updateCurrentNum(11L);
			}	
		});
		
		hexC.setOnClickListener(new View.OnClickListener(){
			public void onClick(View v) {
				Calculator.updateCurrentNum(12L);
			}	
		});
		
		hexD.setOnClickListener(new View.OnClickListener(){
			public void onClick(View v) {
				Calculator.updateCurrentNum(13L);
			}	
		});
		
		hexE.setOnClickListener(new View.OnClickListener(){
			public void onClick(View v) {
				Calculator.updateCurrentNum(14L);
			}	
		});
		
		hexF.setOnClickListener(new View.OnClickListener(){
			public void onClick(View v) {
				Calculator.updateCurrentNum(15L);
			}	
		});
		
		//-----------Operations buttons----------------//
		andBtn.setOnClickListener(new View.OnClickListener(){
			public void onClick(View v) {
				currentNum = 2;
				currentOp = 0;
				operationTxt.setText("AND");
			}	
		});
		
		orBtn.setOnClickListener(new View.OnClickListener(){
			public void onClick(View v) {
				currentNum = 2;
				currentOp = 1;
				operationTxt.setText("OR");
			}	
		});
		
		notBtn.setOnClickListener(new View.OnClickListener(){
			public void onClick(View v) {
				
				operationTxt.setText("NOT");
				if (currentNum == 1)
					num1 = Calculator.logical_not(num1);
				else if (currentNum == 2)
					num2 = Calculator.logical_not(num2);
				else if (currentNum == 0)
					ans = Calculator.logical_not(ans);
				Display.showResults();
			}	
		});
		
		xorBtn.setOnClickListener(new View.OnClickListener(){
			public void onClick(View v) {
				currentNum = 2;
				currentOp = 3;
				operationTxt.setText("XOR");
			}	
		});
		
		slBtn.setOnClickListener(new View.OnClickListener(){
			public void onClick(View v) {
				operationTxt.setText("<<");
				if (currentNum == 1)
					num1 = Calculator.shift_left(num1);
				else if (currentNum == 2)
					num2 = Calculator.shift_left(num2);
				else if (currentNum == 0)
					ans = Calculator.shift_left(ans);
				Display.showResults();
			}	
		});
		
		srBtn.setOnClickListener(new View.OnClickListener(){
			public void onClick(View v) {
				operationTxt.setText(">>");
				if (currentNum == 1)
					num1 = Calculator.shift_right(num1);
				else if (currentNum == 2)
					num2 = Calculator.shift_right(num2);
				else if (currentNum == 0)
					ans = Calculator.shift_right(ans);
				Display.showResults();
			}	
		});
		
		plusBtn.setOnClickListener(new View.OnClickListener(){
			public void onClick(View v) {
				currentNum = 2;
				currentOp = 6;
				operationTxt.setText("+");
			}	
		});
		
		minusBtn.setOnClickListener(new View.OnClickListener(){
			public void onClick(View v) {
				currentNum = 2;
				currentOp = 7;
				operationTxt.setText("-");
			}	
		});
		
		multBtn.setOnClickListener(new View.OnClickListener(){
			public void onClick(View v) {
				currentNum = 2;
				currentOp = 8;
				operationTxt.setText("*");
			}	
		});
		
		divBtn.setOnClickListener(new View.OnClickListener(){
			public void onClick(View v) {
				currentNum = 2;
				currentOp = 9;
				operationTxt.setText("/");
			}	
		});
		
		plus_minusBtn.setOnClickListener(new View.OnClickListener(){
			public void onClick(View v) {
				Calculator.negateCurrentNum();
				Display.showResults();
			}	
		});
		
		equalBtn.setOnClickListener(new View.OnClickListener(){
			public void onClick(View v) {
				if (done == true)
					return;
				switch (currentOp){
				case 0: 
					ans = Calculator.logical_and(num1, num2);
					break;
				case 1:
					ans = Calculator.logical_or(num1, num2);
					break;
				case 3:
					ans = Calculator.logical_xor(num1, num2);
					break;
				case 6:
					if (signed == true)
						ans = Calculator.signed_add(num1, num2);
					else if (signed == false)
						ans = Calculator.unsigned_add(num1, num2);
					break;
				case 7:
					if (signed == true)
						ans = Calculator.signed_sub(num1, num2);
					else if (signed == false)
						ans = Calculator.unsigned_sub(num1, num2);
					break;
				case 8:
					if (signed == true)
						ans = Calculator.signed_mul(num1, num2);
					else if (signed == false)
						ans = Calculator.unsigned_mul(num1, num2);
					break;
				case 9:
					if (signed == true)
						ans = Calculator.signed_div(num1, num2);
					else if (signed == false)
						ans = Calculator.unsigned_div(num1, num2);
					break;
				}//end switch
				done = true;
				currentNum = 0;
				Display.showResults();
				num1 = 0L; num2 = 0L; //reset
				currentNum = 1;
				operationTxt.setText("");
			}	
		});
		
		clearBtn.setOnClickListener(new View.OnClickListener(){
			public void onClick(View v) {
				num1 = 0L;
				num2 = 0L;
				ans = 0L;
				done = true;
				overflow = false;
				operationTxt.setText("");
				currentNum = 1;
				Display.showResults();
			}	
		});
	}//end setOnclickListeners for all buttons
	
	public static void showOverflowAlert(){
		Toast.makeText(context, overflowAlert, duration).show();
	}
	
	public static void showDivByZeroAlert(){
		Toast.makeText(context, divByZeroAlert, duration).show();
	}

}//end class
